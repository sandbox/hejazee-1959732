Description
===========
Drupal implementation of Pasargad Bank Paypaad Payment API, feeding pay-paypaad and uc-paypaad modules.

Requirements
============
  - PHP >= 5.0
  - PHP cURL Extension
  - PHP OpenSSL Extension

API Usage
=========
Read on:
  - http://drupal.org/sandbox/hejazee/1959732

Author and Maintainer
=====================
Drupal 7 (Ubercart 3) version by Ahmad Hejazee (http://drupal.org/user/911168)
Original work by Sepehr Lajevardi (http://drupal.org/user/668010)
